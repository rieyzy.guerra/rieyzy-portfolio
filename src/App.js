import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { useState, useEffect } from 'react'; // Import the useState and useEffect hooks
import Loader from './components/Loader';

import AppNavBar from './components/AppNavBar';
import Contact from './pages/Contact';
import Experience from './pages/Experience';
import Home from './pages/Home';
import About from './pages/About';
import Skills from './pages/Skills';
// import Project from './pages/Project';
import './App.css';

function App() {
  const [loading, setLoading] = useState(true); // State to track loading state

  // Simulate a loading delay (you can replace this with actual data fetching)
  useEffect(() => {
    setLoading(true); // Set loading to true when the path changes

    const loadingTimer = setTimeout(() => {
      setLoading(false);
    }, 3000); // Simulated loading time in milliseconds

    return () => clearTimeout(loadingTimer); // Clean up the timer on unmount
  }, []);

  return (
    <Router>
      {loading ? ( // Show the Loader component if loading is true
        <Loader />
      ) : (
        <>
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/about" element={<About />} />
              <Route path="/experience" element={<Experience />} />
              <Route path="/skills" element={<Skills />} />
              {/* <Route path="/project" element={<Project />} /> */}
              <Route path="/contact" element={<Contact />} />
            </Routes>
          </Container>
        </>
      )}
    </Router>
  );
}

export default App;