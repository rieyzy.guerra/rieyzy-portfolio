import { Triangle } from 'react-loader-spinner'


const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh', // This will make the container take up the full viewport height
        backgroundColor: '#f5f5f5'
    },
};

export default function Loader() {
    return (
        <div style={styles.container}>
            <Triangle
                height="80"
                width="80"
                color="#000000"
                ariaLabel="triangle-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
                id="spinner"
            />
        </div>
    )
}