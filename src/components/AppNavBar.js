import React, { useState } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Switch from "react-switch";

export default function AppNavbar() {
  const [checked, setChecked] = useState(false);

  const handleChange = (checked) => {
    setChecked(checked);
    // Add logic here to toggle your dark mode state or classes
    if (checked) {
      enableDarkMode();
    } else {
      enableLightMode();
    }
  };

  const enableDarkMode = () => {
    document.body.classList.add("dark-mode");
  };

  const enableLightMode = () => {
    document.body.classList.remove("dark-mode");
  };

  

  return (
    <div id="landing-page">
      <Navbar expand="lg" className="bg-dark" id="nav">
        <Container>
          <Navbar.Brand as={NavLink} to="/"><img src="/photo/fullnamelogo.png" width={150} alt="Logo"></img></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" className="navbar-toggle-white"/>
          <Navbar.Collapse className='text-center' id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link as={NavLink} to="/about">About</Nav.Link>
              <span style={{ marginRight: '10px' }}></span>
              <Nav.Link as={NavLink} to="/experience">Experience</Nav.Link>
              <span style={{ marginRight: '10px' }}></span>
              <Nav.Link as={NavLink} to="/skills">Skills</Nav.Link>
              <span style={{ marginRight: '10px' }}></span>
              <Nav.Link as={NavLink} to="/contact">Contact</Nav.Link>
              <span style={{ marginRight: '20px' }}></span>
              <div style={{marginTop: '6px'}}>
              <Switch 
              onChange={handleChange} 
              checked={checked} 
              uncheckedIcon={<img src="/photo/sun.svg" width={26} alt="Light Mode" />}
              checkedIcon={<img src="/photo/moon.svg" width={24} alt="Dark Mode" />}
              />
              </div>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};