import React, { useState } from 'react';
import Fade from 'react-reveal';
import { Row, Col, Container } from 'react-bootstrap';

export default function About() {
    return (
        <Fade>
            <div>
                <h1 className='text-center py-5' id="exp-title">About</h1>
            </div>
            <Container className="d-flex align-items-center justify-content-center">
                <Row className="text-center">
                    <Col md={3} xs={12} className='my-md-0 order-first order-md-last'>
                        <img src="/photo/aboutme.png" className="img-fluid my-5 mx-auto" alt="Rieyzy Guerra" />
                    </Col>
                    <Col md={7} xs={12} className="p-sm-1 my-md-5 order-last order-md-first text-justify">
                        <p className="text-center">
                            Hello, I'm Rieyzy Guera, a web developer with nearly 2 years of professional experience.
                            I've had the opportunity to work on a variety of exciting projects,
                            refining my skills in both front-end and back-end development. I completed a web development bootcamp
                            where I deepened my knowledge and gained practical experience in the latest technologies and best practices.
                            I'm eager to continue my journey as a web developer, seeking opportunities to collaborate on innovative projects
                            and contribute my expertise to create meaningful, user-centric web experiences.
                        </p>
                    </Col>
                </Row>
            </Container>
        </Fade>
    )
}