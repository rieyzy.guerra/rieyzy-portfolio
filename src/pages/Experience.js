import React from "react";
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { FaBriefcase } from 'react-icons/fa'
import { Button, } from 'react-bootstrap';

export default function Experience() {
    return (
        <>
            <div>
                <h1 className='text-center py-5' id="exp-title">Experience</h1>
            </div>
            <VerticalTimeline lineColor="#212529">
                <VerticalTimelineElement className="vertical-timeline-element--work"
                    contentStyle={{ background: '#FFFFFF', color: '#000000' }}
                    iconStyle={{ background: '#000', color: '#fff' }}
                    icon={<FaBriefcase />}
                    date='2023-2024'
                >
                    <h3 className="vertical-timeline-element-title">Regular Developer</h3>
                    <h4 className="vertical-timeline-element-subtitle">Makati</h4>
                    <p>
                        Create and develop a product for the company that integrates seamlessly into the GCash application.
                    </p>
                    <div className="pt-2 exp-button">
                        <Button className="btn btn-secondary" size="sm">React </Button>
                        <Button className="btn btn-secondary mx-2" size="sm">AWS</Button>
                        <Button className="btn btn-secondary" size="sm">MySQL </Button>
                    </div>
                </VerticalTimelineElement>
                <VerticalTimelineElement className="vertical-timeline-element--work"
                    contentStyle={{ background: '#FFFFFF', color: '#000000' }}
                    iconStyle={{ background: '#000', color: '#fff' }}
                    icon={<FaBriefcase />}
                    date='2022-2023'
                >
                    <h3 className="vertical-timeline-element-title">Web Developer</h3>
                    <h4 className="vertical-timeline-element-subtitle">Makati</h4>
                    <p>
                        Create, develop and maintain functional website for the company
                    </p>
                    <div className="pt-2 exp-button">
                        <Button className="btn btn-secondary" size="sm">Angular </Button>
                        <Button className="btn btn-secondary mx-2" size="sm">Laravel </Button>
                        <Button className="btn btn-secondary" size="sm">MySQL </Button>
                    </div>
                </VerticalTimelineElement>
                <VerticalTimelineElement className="vertical-timeline-element--work"
                    contentStyle={{ background: '#FFFFFF', color: '#000000' }}
                    iconStyle={{ background: '#000', color: '#fff' }}
                    icon={<FaBriefcase />}
                    date='2021-2022'
                >
                    <h3 className="vertical-timeline-element-title">WordPress Developer</h3>
                    <h4 className="vertical-timeline-element-subtitle">Freelance</h4>
                    <p>
                        Testing and improving the design of the website. Designing and building the website front-end
                    </p>
                    <div className="pt-2">
                        <Button className="btn btn-secondary" size="sm">Hostinger </Button>
                        <Button className="btn btn-secondary mx-2" size="sm">WordPress </Button>
                    </div>
                </VerticalTimelineElement>
                <VerticalTimelineElement className="vertical-timeline-element--work"
                    contentStyle={{ background: '#FFFFFF', color: '#000000' }}
                    iconStyle={{ background: '#000', color: '#fff' }}
                    icon={<FaBriefcase />}
                    date='2016-2019'
                >
                    <h3 className="vertical-timeline-element-title">IT Assistant</h3>
                    <h4 className="vertical-timeline-element-subtitle">Supervalue, Inc. (SM Supermarket)</h4>
                    <p>
                        Configuration/Installation of new or old Point of Sale (POS). Support and test of POS functionalities

                    </p>
                    <div className="pt-2">
                        <Button className="btn btn-secondary" size="sm">TPlinux </Button>
                        <Button className="btn btn-secondary mx-2" size="sm">SAP </Button>
                        <Button className="btn btn-secondary" size="sm">MySQL </Button>
                    </div>
                </VerticalTimelineElement>
            </VerticalTimeline>
        </>
    )
}