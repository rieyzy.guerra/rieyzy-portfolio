import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Fade } from "react-reveal";
import React from 'react'

export default function Contact() {

    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [message, setMessage] = useState("");

    const [isActive, setIsActive] = useState(false);

    function sendMessage(e){
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/messages/add`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
                name: name,
				email: email,
                mobileNo: mobileNo,
                message: message
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
            if(data){
                alert("Message sent! Thank you for messaging me, talk to you soon!");
                refreshInput();
            } else {
                alert("Please try again!");
                refreshInput();
            }
        })
    }

    const refreshInput = () => {
        setEmail('');
        setName('');
        setMobileNo('');
        setMessage('');
      }

    useEffect(() => {
        if (email !== "" && name !== "" && mobileNo !== "" && message !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, name, mobileNo, message]);
    return (
        <>
            <Fade>
                <div className='contact'>
                    <h1 className='text-center py-5' id="contact-title">Contact</h1>
                </div>

                <div className="contact-container">
                    <div className="contact-form">
                        <Form onSubmit={e => sendMessage(e)}>
                            <h4 className="my-4 text-center" id="form"> Leave me a Message</h4>

                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Control className="name text-center" type="text" placeholder="Name" required
                                    value={name} onChange={e => { setName(e.target.value) }}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
                                <Form.Control className="email text-center" type="email" placeholder="Email" required
                                    value={email} onChange={e => { setEmail(e.target.value) }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
                                <Form.Control className="mobileNo text-center" type="text" placeholder="Phone" required
                                    value={mobileNo} onChange={e => { setMobileNo(e.target.value) }}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput4">
                                <Form.Control className="message text-center" as="textarea" placeholder="Your Message" required
                                    value={message} onChange={e => { setMessage(e.target.value) }}
                                />
                            </Form.Group>

                            {
                                isActive ? <Button variant="success" type="submit" id="submitBtn" className="submit-button"> Submit </Button>
                                    : <Button variant="danger" type="submit" id="submitBtn" className="submit-button" disabled> Submit </Button>
                            }

                        </Form>
                    </div>
                </div>

                <div className='social'>
                    <div className='d-flex justify-content-center pt-4'>
                        <div className='d-flex flex-wrap justify-content-center'>
                            <div className="social-container px-2">
                                <a href='https://www.linkedin.com/in/rieyzy-guerra/'>
                                    <svg
                                        viewBox="0 0 960 1000"
                                        fill="currentColor"
                                    >
                                        <path d="M480 20c133.333 0 246.667 46.667 340 140s140 206.667 140 340c0 132-46.667 245-140 339S613.333 980 480 980c-132 0-245-47-339-141S0 632 0 500c0-133.333 47-246.667 141-340S348 20 480 20M362 698V386h-96v312h96m-48-352c34.667 0 52-16 52-48s-17.333-48-52-48c-14.667 0-27 4.667-37 14s-15 20.667-15 34c0 32 17.333 48 52 48m404 352V514c0-44-10.333-77.667-31-101s-47.667-35-81-35c-44 0-76 16.667-96 50h-2l-6-42h-84c1.333 18.667 2 52 2 100v212h98V518c0-12 1.333-20 4-24 8-25.333 24.667-38 50-38 32 0 48 22.667 48 68v174h98" />
                                    </svg>
                                    <span className='tooltip'>Linkedin</span></a>
                            </div>
                            <div className="social-container px-2">
                                <a href='https://www.instagram.com/iamrizg/' >
                                    <svg
                                        data-name="Layer 1"
                                        viewBox="0 0 24 24"
                                        fill="currentColor"
                                    >
                                        <path d="M12 9.52A2.48 2.48 0 1014.48 12 2.48 2.48 0 0012 9.52zm9.93-2.45a6.53 6.53 0 00-.42-2.26 4 4 0 00-2.32-2.32 6.53 6.53 0 00-2.26-.42C15.64 2 15.26 2 12 2s-3.64 0-4.93.07a6.53 6.53 0 00-2.26.42 4 4 0 00-2.32 2.32 6.53 6.53 0 00-.42 2.26C2 8.36 2 8.74 2 12s0 3.64.07 4.93a6.86 6.86 0 00.42 2.27 3.94 3.94 0 00.91 1.4 3.89 3.89 0 001.41.91 6.53 6.53 0 002.26.42C8.36 22 8.74 22 12 22s3.64 0 4.93-.07a6.53 6.53 0 002.26-.42 3.89 3.89 0 001.41-.91 3.94 3.94 0 00.91-1.4 6.6 6.6 0 00.42-2.27C22 15.64 22 15.26 22 12s0-3.64-.07-4.93zm-2.54 8a5.73 5.73 0 01-.39 1.8A3.86 3.86 0 0116.87 19a5.73 5.73 0 01-1.81.35H8.94A5.73 5.73 0 017.13 19a3.51 3.51 0 01-1.31-.86A3.51 3.51 0 015 16.87a5.49 5.49 0 01-.34-1.81V12 8.94A5.49 5.49 0 015 7.13a3.51 3.51 0 01.86-1.31A3.59 3.59 0 017.13 5a5.73 5.73 0 011.81-.35h6.12a5.73 5.73 0 011.81.35 3.51 3.51 0 011.31.86A3.51 3.51 0 0119 7.13a5.73 5.73 0 01.35 1.81V12c0 2.06.07 2.27.04 3.06zm-1.6-7.44a2.38 2.38 0 00-1.41-1.41A4 4 0 0015 6H9a4 4 0 00-1.38.26 2.38 2.38 0 00-1.41 1.36A4.27 4.27 0 006 9v6a4.27 4.27 0 00.26 1.38 2.38 2.38 0 001.41 1.41 4.27 4.27 0 001.33.26h6a4 4 0 001.38-.26 2.38 2.38 0 001.41-1.41 4 4 0 00.26-1.38v-3-3a3.78 3.78 0 00-.26-1.38zM12 15.82A3.81 3.81 0 018.19 12 3.82 3.82 0 1112 15.82zm4-6.89a.9.9 0 010-1.79.9.9 0 010 1.79z" />
                                    </svg>
                                    <span className='tooltip'> Instagram</span></a>
                            </div>
                            <div className="social-container px-2">
                                <a href='https://www.facebook.com/SHITBRIX1/' >
                                    <svg
                                        viewBox="0 0 24 24"
                                        fill="currentColor"
                                    >
                                        <path d="M12.001 2.002c-5.522 0-9.999 4.477-9.999 9.999 0 4.99 3.656 9.126 8.437 9.879v-6.988h-2.54v-2.891h2.54V9.798c0-2.508 1.493-3.891 3.776-3.891 1.094 0 2.24.195 2.24.195v2.459h-1.264c-1.24 0-1.628.772-1.628 1.563v1.875h2.771l-.443 2.891h-2.328v6.988C18.344 21.129 22 16.992 22 12.001c0-5.522-4.477-9.999-9.999-9.999z" />
                                    </svg>
                                    <span className='tooltip'>Facebook</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <p className='text-center pt-2' id="follow">Follow Me On</p>
                </div>

                <div>
                <p className='text-center p-5'>&copy; 2023 Rieyzy Guerra. All Rights Reserved.</p>
                </div>
            </Fade >

        </>
    )

}