import { Fade } from "react-reveal";

export default function Skills() {
    return (
        <>
            <Fade>
                <div>
                    <h1 className='text-center py-5' id="skills-title">Skills</h1>
                </div>
                <div>
                    <h2 className='text-center mb-3'>I love to learn new things and experiment with new technologies.
                        These are some of the major languages, technologies, tools and platforms I have worked with:</h2>
                </div>

                <div>
                    <h3 className='text-center mb-3 pt-3'>Languages & Databases</h3>
                    <div className="d-flex justify-content-center">
                        <div className="d-flex flex-wrap justify-content-center">
                            <div className="image-container">
                                <img src="/photo/js.png" alt="JavaScript"></img>
                                <p>JavaScript</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/php.png" alt="PHP"></img>
                                <p>PHP</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/mysql.png" alt="MySql"></img>
                                <p>MySql</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/mongodb.png" alt="PHP"></img>
                                <p>MongoDB</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <h3 className='text-center mb-3 pt-5'>Frameworks and Technologies</h3>
                    <div className="d-flex justify-content-center">
                        <div className="d-flex flex-wrap justify-content-center">
                            <div className="image-container">
                                <img src="/photo/expressjs.png" alt="JavaScript"></img>
                                <p>Expressjs</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/nodejs.png" alt="JavaScript"></img>
                                <p>Nodejs</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/react.png" alt="PHP"></img>
                                <p>React</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/angular.png" alt="MySql"></img>
                                <p>Angular</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/laravel.png" alt="MySql"></img>
                                <p>Laravel</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <h3 className='text-center mb-3 pt-5'>Tools and Platforms</h3>
                    <div className="d-flex justify-content-center">
                        <div className="d-flex flex-wrap justify-content-center">
                            <div className="image-container">
                                <img src="/photo/git.png" alt="Git"></img>
                                <p>Git</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/gitlab.png" alt="Gitlab"></img>
                                <p>Gitlab</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/postman.png" alt="Postman"></img>
                                <p>Postman</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/photoshop.png" alt="Photoshop"></img>
                                <p>Photoshop</p>
                            </div>
                            <div className="image-container">
                                <img src="/photo/aws.png" alt="AWS"></img>
                                <p>Amazon Web Services</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Fade>

        </>
    )
}