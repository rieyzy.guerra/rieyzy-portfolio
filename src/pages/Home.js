import { Row, Col, Button } from 'react-bootstrap';
import Fade from 'react-reveal';
import Typewriter from 'typewriter-effect';
import { useState, useEffect } from 'react';

export default function Home() {

    // console.log(homeData);

    const [data, setData] = useState({});

    useEffect(() => {
        fetch('/data/homeData.json') 
            .then((response) => response.json())
            .then(data =>
            // console.log(jsonData)
            setData(data)
            )
            .catch((error) => console.error('Error fetching data:', error));
    }, []);


    return (
        <Fade>
            <Row>
                <Col>
                    <div className="center-content"> {/* Apply the centering styles */}
                        <h2 className="px-2">Hello &nbsp;I&apos;m</h2>
                        <h1 className="home-title text-center">
                            <span>{data.title}</span>
                        </h1>
                        <div id="home-desc" style={{ display: 'flex', alignItems: 'center' }}>
                            <div className="type-writer">
                                <Typewriter
                                    options={{
                                        strings: data.roles,
                                        autoStart: true,
                                        loop: true,
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        </Fade>
    );
}